# cantal: Community dynamics in spatial networks

<!-- badges: start -->
<!-- badges: end -->

## Overview

The package cantal "Community dynamics in spatial networks" is a simulator of metacommunity dynamics in spatial networks experiencing recurrent disturbances. It allows to simulate a wide range of disturbance scenarios as well as potential interspecific heterogeneity in dispersal ability and resistance to pulse disturbances. The simulator can also simulate varying scenarios of disturbance events based on input parameters coming from empirical data (e.g., spatial network structure, species dispersal mode and ability, resistance to disturbances).

## Installation
``` r
install_gitlab("franck.jabot/cantal", host = "git@forgemia.inra.fr")
library(cantal)
```

## Example

First, we create a virtual river network with package OCNet.

``` r
library(OCNet)
set.seed(1)
dimX = 15
dimY = 15
cellsize = 500
thrA = 5*cellsize^2
OCN <- create_OCN(dimX, dimY, cellsize = cellsize, outletPos = 3,expEnergy = 0.1,coolingRate = 0.3)
OCN <- landscape_OCN(OCN, slope0 = 0.05)
OCN <- aggregate_OCN(OCN, thrA = thrA)
```
To visualize the river network: 
``` r
draw_thematic_OCN(rep(1,OCN$RN$nNodes),OCN,drawNodes=T,addLegend=F,cex=1,backgroundColor = NULL)
```
We convert the OCN object to an igraph object:
``` r
graph <- OCN2graph(OCN)
```
Then, we can run the simulation with this call:
``` r
cantal_model(graph=graph, S=100, iK=5000, d=0.1, m=0.25, dm="swimming", d_max = 2000,
    LDD=0.0001, int=0.8, dryingmonths=6, dryingscenario="random", burnsteps=1000,
    nbyears=1)
```

Or with a smaller network for shortest computation time:
``` r
library(cantal)
set.seed(1)
library(OCNet)
dimX = 15
dimY = 15
cellsize = 500
thrA = 5*cellsize^2
OCN <- create_OCN(dimX, dimY, cellsize = cellsize)
OCN <- landscape_OCN(OCN)
OCN <- aggregate_OCN(OCN, thrA = thrA)
draw_thematic_OCN(rep(1,OCN$RN$nNodes),OCN,drawNodes=T,addLegend=F,cex=1,backgroundColor = NULL)
# We convert the OCN object to an igraph object
graph <- OCN2graph(OCN)
# Then, we can run the simulation with this call
cantal_model(graph=graph, S=10, iK=500, d=0.1, m=0.25, dm="swimming", d_max = 2000,
    LDD=0.0001, int=0.8, dryingmonths=6, dryingscenario="random", burnsteps=10,
    nbyears=1)
```

***
